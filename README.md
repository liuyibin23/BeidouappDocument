# BeidouappDocument

#### 介绍

易通星云文档仓库

#### 文档目录结构

```

Market                  市场相关文档
    |-- README.md
    |-- adv             直接提供客户的宣传材料
    |-- customer        客户跟踪记录
    |-- pre_sale        售前资料
    |-- after_sale      售后资料


ResearchDevelop                     研发相关文档
    |-- project                     按项目划分的文档资料
        |-- xxx项目
            |-- README.md
            |-- requirement         需求文档
            |-- develop_document    开发文档
            |-- test                测试文档
            |-- release             交付文档
        |-- yyy项目
            |-- 目录结构同上
    |-- patent                      专利和软件著作权相关资料
    |-- project_application         项目申请相关文档


Production              产品相关文档
    |-- README.md
    |-- design          产品文档中间状态
    |-- document        产品文档市场交付版

```
