1. 操作系统: centos.7.6.18 （可选 rhel 7.2)
   
2. 配置网络ip(例如：固定ip: 192.168.1.185)
   编辑 /etc/sysconfig/network-scripts/ifcfg-eth0 (ifcfg-后面设备名通过ip addr查到)
     BOOTPROTO=none
     IPADDR=192.168.1.185
     NETMASK=255.255.255.0
     GATEWAY=192.168.1.1 
     ONBOOT=yes
   重启 reboot now

3. 挂载ETiiot-7-x86_64-DVD-2.2.1光盘，拷贝内容到/tmp/
4. 挂载CentOS-7-x86_64-DVD-1810光盘到 /media/cdrom
   mkdir /media/cdrom
   mount CentOS-7-x86_64-DVD-1810.iso /media/cdrom 
5. 配置:
  cd /tmp/install_shell
  ./00_Confirm_Configuration.sh
  
    (输入) 本机IP

    (确认)BEIDOUAPP_IMAGE_NAME='dev-env:v1'
  (REDHAT系统)(确认) INSTALL_SYS_VERSION=REDHAT

6. 源配置:
   ./01_cfg_yum_repo.sh
  
7. 基础安装:
  ./02_base_software.sh
8. 文件服务安装:
     ./03_iot-fs_install_tar.sh
9. 物联网服务安装:
  ./04_iot_install.sh
  注意确认下列项需修改:
  Data Directory [/opt/PostgreSQL/10/data]: /data/pg_db
  Password: pgsql
  (语言代码页)Please choose an option[1]: 237   (REDHAT: 763)
  
  
10. 认证安装:
  ./ 05_kc_install.sh

  安装完成后，需远端浏览器访问服务器首页，(https://<ip>),点帐号管理  或 (https://<ip>:8443)

  登录(帐号: sysadmin@beidouapp.com,密码: )
  帐号配置步骤:
  (1) 添加 realm (左侧Master栏)
       Name: beidouapp
       点<Create>
      HTML Display name: 易通星云
      <Save>
      <Login>栏
        User registration : ON
        Email as username: ON
      <Save>
  (2) 添加 Clients （左侧Clients)
     <Create>
    Client ID:  hub
     <Save>
    Access Type: confidential
    Valid Redirect URIs:  https://<ip>:8000/hub/oauth_callback 
    <Save>
   切换Credentials栏:  记录Secret （修改 /opt/install_config.sh)
  (3) 添加 Users (左侧Users)
    <Add user>
   Username:  yt
   Email:   yt@beidouapp.com
   <Save>
   切换Credentials
   Password:
    <Set Password>

11. 配置Secret
   编辑  /opt/install_config.sh
     BEIDOUAPP_KC_CLIENT_SECRET= <Clients Credentials Secret>

12. 科学调试docker安装
   ./06_docker_install.sh

13. 科学计算算法调试平台
     ./07_dev_install.sh

14. 微服务部署平台安装
   ./08_dep_install
   ./09_dev_envs_install.sh

15. （防火墙启用时)确认下列端口可公开访问:
   80，1883，8000，8080，8081，8132，8443，20051，22122
   (微服务) 29050-29100

16. 平台登录
    https://<ip>
     系统帐号： sysadmin@beidouapp.com
   (科学计算) 
      帐号为步骤10创建的User
   
   (物联网平台，微服务地址配置) <ip>:20051


      
