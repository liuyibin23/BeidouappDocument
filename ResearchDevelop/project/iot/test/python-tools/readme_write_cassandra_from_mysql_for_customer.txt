需修改的点:

（1）运行说明

(main.py 23) table_name = table_list[0] (0-4,决定处理哪个表)

(db/sql_tpl) 时间点为处理结束时间
(db/<table_name>_restore.txt) 第一次运行需写入起始时间点，格式为2019-11-01 00:00:00

（2）pm2 启动
pm2 start /root/tangyong/w-cas-normal-speed/main.py --interpreter /beidouapp/bda-pkgs/bin/python3.7 --name='SQL2TS' --no-autorestart --log=/root/tangyong/logs_writecassandra/log_tb_gps_data_1.txt

（3）内部调整说明

  运行速度 (taskmanage.py) 
       self._db_read_ratio = 
       _set_db_read_speed_strategy

  进程，线程, 读缓存（main.py 29)
      process_nums
      thread_nums
      queue_size

