var mqtt = require('mqtt');
//var dev_access_token="hi58qn98P4155uECwfRZ";
var client = mqtt.connect('mqtt://192.168.1.95:1883', {
    //username: dev_access_token
    username: process.env.TMQTTTOKEN
});

roundswith_stat = true;
knob_stat = "36.0";
lastscmd = "";

client.on('connect', function () {
    console.log('connected');
    //client.subscribe('v1/devices/me/rpc/response/+');
    client.subscribe('v1/devices/me/rpc/request/+');
    var requestId = 1;
    var request = {
        "method": "getTwwval",
        "params": {}
    };
    //client.publish('v1/devices/me/rpc/request/' + requestId, JSON.stringify(request));
});

client.on('message', function (topic, message) {
    console.log('receive.topic: ' + topic + '   time: ' + (new Date()).toString());
    console.log('receive.body: ' + message.toString());
    var rpcmsg = JSON.parse(message);
    var ldv = {};
    var jrep=topic.replace('request','response');
    if(rpcmsg.method == 'checkStatus'){
        ldv.value = roundswith_stat;//1;//(Math.random() > 0.7) ? 1 : 0;
        
        console.log("resp:" + JSON.stringify(ldv));
        client.publish(jrep, JSON.stringify(ldv));
        client.publish('v1/devices/me/attributes', JSON.stringify(ldv));
    }
    if(rpcmsg.method == 'getValue'){
      ldv.value = roundswith_stat;
      //client.publish(jrep, JSON.stringify(ldv));
      client.publish(jrep, JSON.stringify(ldv.value));
    }
    if(rpcmsg.method == 'setValue'){
        roundswith_stat = rpcmsg.params ? 1 : 0;
        ldv.value = roundswith_stat;
        client.publish(jrep, JSON.stringify(ldv));
        client.publish('v1/devices/me/attributes', JSON.stringify(ldv));
    }
    if(rpcmsg.method == 'getValue2'){ //widget knob control
        ldv.value = knob_stat;
        client.publish(jrep, JSON.stringify(ldv.value));
      }
      if(rpcmsg.method == 'setValue2'){ //widget knob control
         knob_stat = rpcmsg.params ;
	      console.log("setValue2 data: " + rpcmsg.params);
          ldv.value = knob_stat;
          client.publish(jrep, JSON.stringify(ldv));
          //client.publish('v1/devices/me/attributes', JSON.stringify(ldv));
      }
      if(rpcmsg.method == 'getTermInfo'){
          var terminfis = {
            ok: true,
            platform: "Linux",
            type: "x86_64",
            release: "4.9.168"
          };
          client.publish(jrep, JSON.stringify(terminfis));
      }
      if(rpcmsg.method == 'sendCommand'){  //widget RPC remote shell
          if(rpcmsg.params.command == 'getValue2'){
            ldv.value = knob_stat;
            lastscmd = 'getValue2';
          }else {
            lastscmd = 'unknown cmd';
          }
          var ressnd = {
            "ok": true
          }

          /*var rescmd = {
            "done": "true",
            "data": [{"stdout": "Hello"}]
          }*/
          client.publish(jrep, JSON.stringify(ressnd));
      }
      if(rpcmsg.method == 'getCommandStatus'){
        var lcmdrt = {
          "done": "true",
          "data" : []
        }
        if(lastscmd == 'getValue2'){
           var da = {};
           da.knobstat = knob_stat;
          
            lcmdrt.data[0]= {"stdout": JSON.stringify(da)};
        }else {
           lcmdrt.data[0]= {"stdout": lastscmd};
        }
        client.publish(jrep, JSON.stringify(lcmdrt));

      }

      if(rpcmsg.method == 'myRemoteMethod1') //widget RPC debug terminal, when user input: myRemoteMethond1
      {
        var tres = {
          "yourmethod": rpcmsg.method,
          "yourparams": rpcmsg.params
        }
        client.publish(jrep, JSON.stringify(tres));
      }

      if(rpcmsg.method == 'testonewayrpcfromserver')
      {
         console.log("received testonewayrpcfromserver rpc call");
      }


      if(rpcmsg.method == 'testtwowayrpcfromserver')
      {
         console.log("received testtwowayrpcfromserver rpc call");
         var tres = {
           "receivedmethod": rpcmsg.method,
           "receivedargu": rpcmsg.params
         }

         client.publish(jrep, JSON.stringify(tres));
      }
});
