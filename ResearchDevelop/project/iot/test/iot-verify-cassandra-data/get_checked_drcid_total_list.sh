startstr=$1
endstr=$2
listf=$3
#echo "from $startstr to $endstr"
#echo "-----"
if [ "a$3" = "a" ] ; then
echo "Usage: cmd start end listfile"
echo " Example:  $0  1 62  ./total_checked_drcid"
exit
fi


index=$((startstr))
endn=$((endstr))

let records=0

rm -f listf

while [ $index -lt $endn ]
do

echo "search file: ./test-logs/logs-test-$index"
cat ./test-logs/logs-test-$index | awk  '/^theonedevice:/{print $0}' | awk -F " " '{print $4}' >> $listf 



#echo "ss: $curstr"

let index+=1
done

echo "--result--"
wc -l $listf

echo "-----"
echo "maybe use it for create new drcid file:"
echo " cat drcid_fin_lists | xargs -i sed -i '/^..{}/d' drcid2deviceid"
echo " split --lines=2000 --numeric-suffixes=1  <file>  <new file prefix>"

#echo "tot:$records"
