package com.testdata.checkdata;

public class gpsData {
    long id;
    String drc_id;
    String tractor_id;
    String version;
    double latitude;
    double longitude;
    float altitude;
    float speed;
    float azimuth;
    String total_mileage;
    String single_mileage;
    String cellid;
    String lac;
    String mcc;
    String mnc;
    float power;
    float extpower;
    String gps_num;

    long ts; //create_time;
    String create_time;
    long send_time;
    boolean send_time_valid;
    long partition;

    boolean skip; //ignore flag

    boolean notequal;  //value is not equal flag
}
