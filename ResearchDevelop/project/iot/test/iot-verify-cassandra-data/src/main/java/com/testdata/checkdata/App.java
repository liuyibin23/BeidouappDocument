package com.testdata.checkdata;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

//import org.json.JSONObject;

import java.io.*;


import com.datastax.driver.core.*;
//import java.sql.*;

//import com.datastax.driver.core.Cluster;
//import com.datastax.driver.core.ResultSet;
//import com.datastax.driver.core.Session;


/**
 * Hello world!
 *
 */
public class App 
{
    public static boolean verifyGpsData(DataCassandra csd, gpsData gdt, String deviceid) throws Exception
    {
        JSONObject jsnVal;
        boolean result = true;
        gdt.notequal = false;

        String query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='latitude' and partition=%d and ts=%d";
        csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
        List<Object> tlis = csd.getData();
        if(tlis.size() != 0){
            //System.out.println("found latitude!");
            jsnVal = (JSONObject)tlis.get(0);
            double val = ((Number)jsnVal.get("dbl_v")).doubleValue();
            //System.out.println( "the latitude:" +  val + " ts:" + gdt.ts);
            if(val != gdt.latitude) {
                System.out.println("the latitude:" +  val + " cmd v: " + gdt.latitude);
                result = false;
                gdt.notequal = true;
            }

        }else{
            result = false;
        }

        //System.out.println( "1theee res:" +  result + " ts:" + gdt.ts);

        query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='longitude' and partition=%d and ts=%d";
        csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
        tlis = csd.getData();
        if(tlis.size() != 0){
            //jsnVal = (JSONObject)tlis.get(0);
            double val = ((Number)((JSONObject)tlis.get(0)).get("dbl_v")).doubleValue();
           // System.out.println( "the longitude:" +  val + " ts:" + gdt.ts);
            if(val != gdt.longitude){
                System.out.println( "the longitude:" +  val + " cmd v:" + gdt.longitude);
                result = false;
                gdt.notequal = true;
            }

        }else{
            result = false;
        }

        //System.out.println( "2theee res:" +  result + " ts:" + gdt.ts);


        query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='altitude' and partition=%d and ts=%d";
        csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
        tlis = csd.getData();
        if(tlis.size() != 0){
            //jsnVal = (JSONObject)tlis.get(0);
            double val = ((Number)((JSONObject)tlis.get(0)).get("dbl_v")).doubleValue();
            //System.out.println( "the altitude:" +  val + " ts:" + gdt.ts);

            if(Math.abs(val - gdt.altitude) > 0.000001 * Math.abs(gdt.altitude) ){
                System.out.println("the altitude:" +  val + " cmd vv: " + gdt.altitude);
                result = false;
                gdt.notequal = true;
            }

        }else{
            result = false;
        }

        //System.out.println( "4theee res:" +  result + " ts:" + gdt.ts);

        query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='speed' and partition=%d and ts=%d";
        csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
        tlis = csd.getData();
        if(tlis.size() != 0){
            //jsnVal = (JSONObject)tlis.get(0);
            double val = ((Number)((JSONObject)tlis.get(0)).get("dbl_v")).doubleValue();
            //System.out.println( "the speed:" +  val + " ts:" + gdt.ts);
            if(Math.abs(val - gdt.speed) > 0.000001 * Math.abs(gdt.speed) ) {
                System.out.println( "the speed:" +  val + " cmd v: " + gdt.speed);
                result = false;
                gdt.notequal = true;
            }

        }else{
            result = false;
        }

        //System.out.println( "5theee res:" +  result + " ts:" + gdt.ts);

        query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='azimuth' and partition=%d and ts=%d";
        csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
        tlis = csd.getData();
        if(tlis.size() != 0){
            //jsnVal = (JSONObject)tlis.get(0);
            double val = ((Number)((JSONObject)tlis.get(0)).get("dbl_v")).doubleValue();
            //System.out.println( "the azimuth:" +  val + " ts:" + gdt.ts);
            if(Math.abs(val - gdt.azimuth) > 0.000001 * Math.abs(gdt.azimuth) ) {
                System.out.println( "the azimuth:" +  val + " cmd vv:" + gdt.azimuth);
                result = false;
                gdt.notequal = true;
            }

        }else{
            result = false;
        }

        //System.out.println( "6theee res:" +  result + " ts:" + gdt.ts);

        if(gdt.total_mileage != null){
            query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='total_mileage' and partition=%d and ts=%d";
            csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
            tlis = csd.getData();
            if(tlis.size() != 0){
                //jsnVal = (JSONObject)tlis.get(0);
                String val = (String)((JSONObject)tlis.get(0)).get("str_v");
                //System.out.println( "the total_mileage:" +  val + " ts:" + gdt.ts);
                if(!val.equals(gdt.total_mileage) ){
                    System.out.println( "the total_mileage:" +  val + " cmd vv:" + gdt.total_mileage);
                    result = false;
                    gdt.notequal = true;
                }

            }else{
                result = false;
            }
        }

        if(gdt.single_mileage != null){
            query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='single_mileage' and partition=%d and ts=%d";
            csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
            tlis = csd.getData();
            if(tlis.size() != 0){
                //jsnVal = (JSONObject)tlis.get(0);
                String val = (String)((JSONObject)tlis.get(0)).get("str_v");
                //System.out.println( "the single_mileage:" +  val + " ts:" + gdt.ts);
                if(!val.equals(gdt.single_mileage) ){
                    System.out.println( "the single_mileage:" +  val + " cmd vv:" + gdt.single_mileage);
                    result = false;
                    gdt.notequal = true;
                }

            }else{
                result = false;
            }
        }

        if(gdt.cellid != null){
            query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='cellid' and partition=%d and ts=%d";
            csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
            tlis = csd.getData();
            if(tlis.size() != 0){
                //jsnVal = (JSONObject)tlis.get(0);
                String val = (String)((JSONObject)tlis.get(0)).get("str_v");
                //System.out.println( "the cellid:" +  val + " ts:" + gdt.ts);
                if(!val.equals(gdt.cellid) ) {
                    System.out.println( "the cellid:" +  val + " cmd vv:" + gdt.cellid);
                    result = false;
                    gdt.notequal = true;
                }

            }else{
                result = false;
            }
        }

        if(gdt.lac != null){
            query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='lac' and partition=%d and ts=%d";
            csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
            tlis = csd.getData();
            if(tlis.size() != 0){
                //jsnVal = (JSONObject)tlis.get(0);
                String val = (String)((JSONObject)tlis.get(0)).get("str_v");
                //System.out.println( "the lac:" +  val + " ts:" + gdt.ts);
                if(!val.equals(gdt.lac) ) {
                    System.out.println( "the lac:" +  val + " cmd vv:" + gdt.lac);
                    result = false;
                    gdt.notequal = true;
                }

            }else{
                result = false;
            }
        }

        if(gdt.mcc != null){
            query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='mcc' and partition=%d and ts=%d";
            csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
            tlis = csd.getData();
            if(tlis.size() != 0){
                //jsnVal = (JSONObject)tlis.get(0);
                String val = (String)((JSONObject)tlis.get(0)).get("str_v");
                //System.out.println( "the mcc:" +  val + " ts:" + gdt.ts);
                if(!val.equals(gdt.mcc) ){
                    System.out.println( "the mcc:" +  val + " cmd vv:" + gdt.mcc);
                    result = false;
                    gdt.notequal = true;
                }

            }else{
                result = false;
            }
        }

        if(gdt.mnc != null){
            query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='mnc' and partition=%d and ts=%d";
            csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
            tlis = csd.getData();
            if(tlis.size() != 0){
                //jsnVal = (JSONObject)tlis.get(0);
                String val = (String)((JSONObject)tlis.get(0)).get("str_v");
                //System.out.println( "the mnc:" +  val + " ts:" + gdt.ts);
                if(!val.equals(gdt.mnc) ){
                    System.out.println( "the mnc:" +  val + " cmd vv:" + gdt.mnc);
                    result = false;
                    gdt.notequal = true;
                }

            }else{
                result = false;
            }
        }

        query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='power' and partition=%d and ts=%d";
        csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
        tlis = csd.getData();
        if(tlis.size() != 0){
            //jsnVal = (JSONObject)tlis.get(0);
            double val = ((Number)((JSONObject)tlis.get(0)).get("dbl_v")).doubleValue();
            //System.out.println( "the power:" +  val + " ts:" + gdt.ts);
            if(Math.abs(val - gdt.power) > 0.000001 * Math.abs(gdt.power) ) {
                System.out.println( "the power:" +  val + " cmd vv:" + gdt.power);
                result = false;
                gdt.notequal = true;
            }

        }else{
            result = false;
        }

        //System.out.println( "7theee res:" +  result + " ts:" + gdt.ts);

        query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='extpower' and partition=%d and ts=%d";
        csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
        tlis = csd.getData();
        if(tlis.size() != 0){
            //jsnVal = (JSONObject)tlis.get(0);
            double val = ((Number)((JSONObject)tlis.get(0)).get("dbl_v")).doubleValue();
            //System.out.println( "the extpower:" +  val + " ts:" + gdt.ts);
            if(Math.abs(val - gdt.extpower) > 0.000001 * Math.abs(gdt.extpower) ) {
                System.out.println( "the extpower:" +  val + " cmd vv:" + gdt.extpower);
                result = false;
                gdt.notequal = true;
            }

        }else{
            result = false;
        }

        //System.out.println( "8theee res:" +  result + " ts:" + gdt.ts);

        if(gdt.gps_num != null){
            query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='gps_num' and partition=%d and ts=%d";
            csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
            tlis = csd.getData();
            if(tlis.size() != 0){
                //jsnVal = (JSONObject)tlis.get(0);
                String val = (String)((JSONObject)tlis.get(0)).get("str_v");
                //System.out.println( "the gps_num:" +  val + " ts:" + gdt.ts);
                if(!val.equals(gdt.gps_num) ) {
                    System.out.println( "the gps_num:" +  val + " cmd vv:" + gdt.gps_num);
                    result = false;
                    gdt.notequal = true;
                }

            }else{
                result = false;
            }
        }

        //System.out.println( "9theee res:" +  result + " ts:" + gdt.ts);

        if(gdt.send_time_valid){
            query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='send_time' and partition=%d and ts=%d";
            csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
            tlis = csd.getData();
            if(tlis.size() != 0){
                //jsnVal = (JSONObject)tlis.get(0);
                long val = ((Number)((JSONObject)tlis.get(0)).get("long_v")).longValue();
                //System.out.println( "the send_time:" +  val + " ts:" + gdt.ts);
                if(val != gdt.send_time){
                    System.out.println( "the send_time:" +  val + " cmd vv:" + gdt.send_time);
                    result = false;
                    gdt.notequal = true;
                }

            }else{
                result = false;
            }
        }

        if(gdt.version != null) {
            query = "SELECT * FROM ts_kv_cf where entity_type='DEVICE' and entity_id=%s and key='version' and partition=%d and ts=%d";
            csd.execute(String.format(query, deviceid, gdt.partition, gdt.ts));
            tlis = csd.getData();
            if(tlis.size() != 0){
                String val = (String)((JSONObject)tlis.get(0)).get("str_v");
                //System.out.println( "the version:" +  val + " ts:" + gdt.ts);
                if(!val.equals(gdt.version) ){
                    System.out.println( "the version:" +  val + " cmd vv:" + gdt.version);
                    result = false;
                    gdt.notequal = true;
                }
            }else{
                result = false;
            }

        }

        return result;

    }

    public static void main( String[] args ) throws Exception //IOException
    {

        System.out.println( "config:"  + args[0]);


        String query = "SELECT * FROM ts_kv_cf limit 10";
        //Cluster cluster = Cluster.builder().addContactPoint("192.168.1.202").build();

        /*Metadata metadata = cluster.getMetadata();

        System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());
        for (Host host: metadata.getAllHosts()) {
            System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
        }*/


        //Session session = cluster.connect("beidouapp");
        //ResultSet rs = session.execute(query);
        //System.out.println(result.all());

        /*List<Object> list = new ArrayList<Object>();
        ColumnDefinitions rscd = rs.getColumnDefinitions();
        int count = rscd.size();
        Map<String, String> reflect = new HashMap<String, String>();
        for (int i = 0; i < count; i++) {
            String column = rscd.getName(i);
            System.out.printf("comlumn %d: %s\n", i, column);
            String tcolumn = column.replaceAll("_", "");

            reflect.put(column, column);

        }
        for (Row row : rs.all()) {
            JSONObject obj = new JSONObject();
            for (String column : reflect.keySet()) {
                String key = reflect.get(column);
                Object value = row.getObject(column);
                System.out.println(value);
                obj.put(key, value);
            }
            Object object = obj;

            list.add(object);
        }*/





        /*while(result.next()) {
            System.out.printf("val: %s", result.getString(1));
        }*/

        //ResultSetMetaData rsmd = result.getMetaData();
        //for (int i = 1; i <= rsmd.getColumnCount(); i++) { String name = rsmd.getColumnName(i);  ystem.out.printf("column %d: %s", i, name); }
        //session.close();
        //cluster.close();

             JsonConfig cfg = new JsonConfig();
       // "/work/BeidouappDocument/ResearchDevelop/project/iot/test/iot-verify-cassandra-data/drcid-deviceid-test"
        Map<String,String>  idlst = cfg.getConfig(args[0]);

        System.out.println("The size of the id map is (lookup device summu): " + idlst.size());
        Set entrySet = idlst.entrySet();
        Iterator it = entrySet.iterator();


        /*System.out.println(idlst.get("869587038672867"));
        System.out.println(idlst.get("863987039905405"));
        System.out.println(idlst.get("860675040452862"));*/


        /*DataCassandra dtCas = new DataCassandra();
        dtCas.connect();
        dtCas.getinfo();
        dtCas.execute(query);
        List<Object> tlis = dtCas.getData();
        dtCas.close();*/

        DataCassandra dtCas = new DataCassandra();
        dtCas.connect();

        gpsData theGpsd = new gpsData();
        int total_rows = 0;
        int tdev_rows = 0;
        int tcurret_rows = 0;
        int skip_rows = 0;
        int vfail_rows = 0;
        int perlimits = 2000;
        int dev_indexn = 0;

        int thedev_records_tt = 0;
        int thedev_skips = 0;
        int thedev_failled = 0;
        int thedev_miss_failed = 0;
        String thedev_fmiss_tm = "";
        long thedev_fmiss_ts = 2532685860000L;

           // "select * from tb_gps_data where drc_id='%s' and create_time IS NULL limit %d,%d"
        String sqlgps = new String("select * from tb_gps_data where drc_id='%s' and create_time <'2020-04-09 22:00:00' limit %d,%d");

        DataMySQL sdt = new DataMySQL();
        sdt.connet();

        while(it.hasNext()){
             thedev_records_tt = 0;
            thedev_skips = 0;
             thedev_failled = 0;
             thedev_miss_failed = 0;
            thedev_fmiss_tm = "";
            thedev_fmiss_ts = 2532685860000L;

            dev_indexn++;
            Map.Entry me = (Map.Entry)it.next();
            System.out.println("drcid Keykv is: "+me.getKey() +
                    " & " +
                    " deviceid valuevv is: "+me.getValue());
            int curpos = 0;
            tdev_rows = 0;
            while(true) {
                //sdt.search(10, "select * from tb_gps_data where drc_id='862952026635424' limit 0,10");
                tcurret_rows = 0;
                String thissql = String.format(sqlgps, me.getKey().toString(), curpos, perlimits);
                sdt.requestSql(thissql);
                if(sdt.errstop) break;
                while((!sdt.errstop) && sdt.nextGPS(theGpsd)) {
                    thedev_records_tt++;
                    tcurret_rows++;

                   // System.out.println("id:" + theGpsd.id); //record row for mysql

                    if(theGpsd.skip) {
                        skip_rows++;
                        thedev_skips++;
                    }
                    else {

                        if(verifyGpsData(dtCas, theGpsd, me.getValue().toString())){

                        }else {
                            vfail_rows++;
                            thedev_failled++;
                            if(theGpsd.notequal) {

                            } else {
                                thedev_miss_failed++;
                                if(theGpsd.ts < thedev_fmiss_ts){
                                    thedev_fmiss_ts = theGpsd.ts;
                                    thedev_fmiss_tm = theGpsd.create_time;
                                }

                            }
                            System.out.println("vvvverify failed " + vfail_rows + " id: " + theGpsd.id + " ts: " + theGpsd.ts + " partn: " + theGpsd.partition + " drcid: " + me.getKey() + " did: " + me.getValue() + " valflg: " + theGpsd.notequal + " creattime: " + theGpsd.create_time );
                        }

                    }
                }
                System.out.println("mysql rows:" + tcurret_rows);
                tdev_rows += tcurret_rows;
                if(sdt.errstop || tcurret_rows == 0) break;
                curpos += perlimits;

            }

            System.out.println("theonedevice: " + dev_indexn + " = " + me.getKey() + " rows: " + thedev_records_tt   + " skips: " + thedev_skips + " fail: " + thedev_failled + " miss: " + thedev_miss_failed + " first misstm= " + thedev_fmiss_tm + " --total records: " + total_rows + " currtime: " + new Date());
            total_rows += tdev_rows;
            if(sdt.errstop) break;

        }

        System.out.println("config file is: " + args[0]);

        System.out.println("total rows: " + total_rows +" total skip: " + skip_rows );

        if(vfail_rows != 0) System.out.println(" thethetot failed: " + vfail_rows);

        if(sdt.errstop) System.out.println("thegpsdatamysql err");

        sdt.close();
        dtCas.close();
    }
}
