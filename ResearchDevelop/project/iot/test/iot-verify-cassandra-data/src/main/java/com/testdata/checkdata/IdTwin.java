package com.testdata.checkdata;

public class IdTwin {
    String drcid;
    String deviceid;
    public void setDrcid(String id) {
        this.drcid = id;
    }
    public void setDeviceid(String id) {
        this.deviceid = id;
    }
    public String toString() {
        return "did:" + this.drcid + ",nid:" + this.deviceid;
    }
}
