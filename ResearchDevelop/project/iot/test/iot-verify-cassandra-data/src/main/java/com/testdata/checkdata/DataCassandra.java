package com.testdata.checkdata;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import com.datastax.driver.core.*;

import com.datastax.driver.core.exceptions.*;

import java.util.concurrent.TimeUnit;

public class DataCassandra {

    Cluster cluster;
    Metadata metadata;
    Session session;
    ResultSet rs;
    void connect(){
        SocketOptions options = new SocketOptions();
        options.setConnectTimeoutMillis(30000);
        options.setReadTimeoutMillis(300000);
        options.setTcpNoDelay(true);
        // 127.0.0.1   10.153.46.242
        cluster = Cluster.builder().addContactPoint("10.153.46.242").withSocketOptions(options).build();  //192.168.1.202  //192.168.1.185
        metadata = cluster.getMetadata();
        session = cluster.connect("beidouapp");
    }

    boolean reconnect() {
        boolean reconstat = false;
        int recon_max = 10;
        while (recon_max > 0 && !reconstat){
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(5));
            }catch(Exception e){

            }

            reconstat = true;
            recon_max--;

            try {
                session = cluster.connect("beidouapp");
            }catch(Exception e){
                reconstat = false;
            }

        }
        return reconstat;
    }

    void getinfo(){

        System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());
        for (Host host: metadata.getAllHosts()) {
             System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
        }
    }

    void execute(String sql) throws Exception {
        boolean contexe = false;
        Exception lastException = new IllegalStateException();
        try {
            rs = session.execute(sql);
        } catch (TransportException ex){
            lastException = ex;
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(20));
            }catch(Exception e){
            }

            contexe = reconnect();
            if(!contexe) throw lastException;

        }catch (NoHostAvailableException ex){
            lastException = ex;
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(20));
            }catch(Exception e){

            }
            contexe = reconnect();
            if(!contexe) throw lastException;
        }catch (ReadTimeoutException ex) {
            System.out.println("cassandra ReadTimeoutException when exe: " + sql);
            throw ex;
        }
        finally {
            if(contexe) {
                rs = session.execute(sql);
            }
        }
    }

    List<Object> getData() {
        List<Object> list = new ArrayList<Object>();
        ColumnDefinitions rscd = rs.getColumnDefinitions();
        int count = rscd.size();
        Map<String, String> reflect = new HashMap<String, String>();
        for (int i = 0; i < count; i++) {
            String column = rscd.getName(i);
            //System.out.printf("comlumn %d: %s\n", i, column);
            String tcolumn = column.replaceAll("_", "");

            reflect.put(column, column);

        }
        for (Row row : rs.all()) {
            JSONObject obj = new JSONObject();
            for (String column : reflect.keySet()) {
                String key = reflect.get(column);
                Object value = row.getObject(column);
                //System.out.println(value);
                obj.put(key, value);
            }
            Object object = obj;

            list.add(object);
        }

        //rs.close();
        return list;
    }

    void close() {
        session.close();
        cluster.close();
    }

}
