package com.testdata.checkdata;

import java.lang.*;
import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.text.SimpleDateFormat;

public class DataMySQL  {
    Connection conn = null;
    PreparedStatement statement = null;
    Long id = 0L;
    Integer loops = 0;
    boolean errstop = false;
    ResultSet therst = null;
    Integer getLoops()
    {
        return loops;
    }
    void connet() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            // 10.153.58.93:3306    106.74.152.2:26093
            conn = DriverManager.getConnection("jdbc:mysql://10.153.58.93:3306/tractor_platform?autoReconnect=true&useSSL=false",
                    "fengyunhe", "U5nCQY72MyX8");
        }catch(SQLException e){
            System.out.println("con mysql err");
        }finally {

        }
    }

    void requestSql(String sqlstr) {
        try {
            statement = conn.prepareStatement(sqlstr);
            therst = statement.executeQuery();
            //statement.close();
        }catch(SQLException e){
            errstop = true;
        }finally {

        }
    }

    boolean nextGPS(gpsData dt) {
        boolean resl = false;
        try{
        if(therst.next()){
            dt.skip = false;
            dt.send_time_valid = true;
            dt.altitude = therst.getFloat(9);
            dt.azimuth = therst.getFloat(11);
            dt.cellid = therst.getString(14);
            dt.drc_id = therst.getString(2);
            dt.extpower = therst.getFloat(19);
            dt.gps_num = therst.getString(20);
            dt.id = therst.getLong(1);
            dt.lac = therst.getString(15);
            dt.latitude = therst.getDouble(5);
            dt.latitude = (therst.getInt(6) == 1) ? dt.latitude : -dt.latitude;
            dt.longitude = therst.getDouble(7);
            dt.longitude = (therst.getInt(8) == 1) ? dt.longitude : -dt.longitude;
            dt.mcc = therst.getString(16);
            dt.mnc = therst.getString(17);
            dt.power = therst.getFloat(18);
            dt.single_mileage = therst.getString(13);
            dt.speed = therst.getFloat(10);
            dt.total_mileage = therst.getString(12);
            dt.tractor_id = therst.getString(3);
            dt.version = therst.getString(4);
            if(therst.getTimestamp(21) == null) {
                //System.out.println("create time null");
                dt.skip = true;
            }else {
                SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                Date tptndate = new java.util.Date(therst.getTimestamp(21).getTime());
                dt.create_time = dfm.format(tptndate);
                dt.ts = tptndate.UTC(tptndate.getYear(), tptndate.getMonth(), tptndate.getDate(), tptndate.getHours(), tptndate.getMinutes(), tptndate.getSeconds());
                dt.partition = tptndate.UTC(tptndate.getYear(), tptndate.getMonth(), 1, 0, 0, 0);

                if(therst.getTimestamp(22) == null) {
                    //dt.send_time = 0;
                    dt.send_time_valid = false;
                }else {
                    tptndate = new java.util.Date(therst.getTimestamp(22).getTime());
                    dt.send_time = tptndate.UTC(tptndate.getYear(), tptndate.getMonth(), tptndate.getDate(), tptndate.getHours(), tptndate.getMinutes(), tptndate.getSeconds());
                }
            }
            resl = true;
        }else {
            therst.close();
            statement.close();
        }
        }catch(SQLException e){
            errstop = true;
        }finally {

        }
        return resl;
    }

    void search(Integer limitcnts, String sqlstr){
        Integer pcounts = limitcnts;
        //String sql2 = "insert into info(date,time,spot,air_temperature,air_humidity,soil_temperature,soil_humidity, illuminance, wind_speed, wind_direction, PH, salinity ) values (?,?,?,?,?,?,?,?,?,?,?,?)";
        String sql1 = "select * from tb_gps_data where id > %d limit ";
        String sql2 = "select * from tb_gps_data limit ?";
        String sql3 = "select * from tb_gps_data limit 10";
        ResultSet rst = null;
        Integer hascounts = 0;
        String sqlf = String.format(sql1, id);
        try {
            statement = conn.prepareStatement(sqlstr);
            //statement.setString(1, "10");
            //statement.execute();
            rst = statement.executeQuery();
            loops++;
            System.out.println("search loop " + loops);
            while(rst.next()) {
                hascounts++;
                System.out.println("daddaaaaa" + rst.getObject(1).getClass().getName() + "id:");
                id = rst.getLong(1);
                System.out.print(rst.getLong(1)); //id
                //if(rst.getObject(1).toString().equals("232949626695716871") ) System.out.println ("find");
                System.out.print("\t"+rst.getString(2)); //drc_id
                System.out.print("\t"+rst.getString(3)); //tractor_id
                System.out.print("\t"+rst.getString(4)); //version
                System.out.println("\t"+ String.valueOf(rst.getDouble(5))); //latitude

                System.out.print("\t"+ rst.getInt(6));//latitude_flag
                System.out.print("\t"+ String.valueOf(rst.getDouble(7))); //longitude
                System.out.print("\t"+ rst.getInt(8)); //longitude_flag
                System.out.print("\t"+ String.valueOf(rst.getFloat(9)));//altitude
                System.out.println("\t"+ String.valueOf(rst.getFloat(10)));//speed

                System.out.print("\t"+ String.valueOf(rst.getFloat(11))); //azimuth
                System.out.print("\t"+rst.getString(12)); //total_mileage
                System.out.print("\t"+rst.getString(13)); //single_mileage
                System.out.print("\t"+rst.getString(14)); //cellid
                System.out.println("\t"+rst.getString(15)); //lac

                System.out.print("\t"+rst.getString(16));  //mcc
                System.out.print("\t"+rst.getString(17));  //mnc
                System.out.print("\t"+ String.valueOf(rst.getFloat(18))); //power
                System.out.print("\t"+ String.valueOf(rst.getFloat(19))); //extpower
                System.out.println("\t"+rst.getString(20)); //gps_num

                System.out.print("\t"+rst.getTimestamp(21)); //create_time
                System.out.println("\t"+rst.getTimestamp(21).getTimezoneOffset());
                Date tptndate = new java.util.Date(rst.getTimestamp(21).getTime());
                System.out.print("tptndate: " + tptndate);
                /*tptndate.setDate(1);
                tptndate.setHours(0);
                tptndate.setMinutes(0);
                tptndate.setSeconds(0);*/
                System.out.print("tptndate2: " + tptndate);
                System.out.print("tptndate2 partition: " + tptndate.UTC(tptndate.getYear(), tptndate.getMonth(), 1, 0, 0, 0 ));
                System.out.print("tptndate utc time: " + tptndate.UTC(tptndate.getYear(), tptndate.getMonth(), tptndate.getDate(), tptndate.getHours(), tptndate.getMinutes(), tptndate.getSeconds() ));



                System.out.println("\t"+rst.getTimestamp(22)); //send_time


            }

            if(pcounts != hascounts){
                errstop = true;
                System.out.println("Warning: counts below limits " + String.valueOf(hascounts));
            }

            statement.close();
        }catch(SQLException e){

        }finally {

        }
    }
    void searchAndPrint(Integer limitcnts){
        Integer pcounts = limitcnts;
        //String sql2 = "insert into info(date,time,spot,air_temperature,air_humidity,soil_temperature,soil_humidity, illuminance, wind_speed, wind_direction, PH, salinity ) values (?,?,?,?,?,?,?,?,?,?,?,?)";
        String sql1 = "select * from tb_gps_data where id > %d limit ";
        String sql2 = "select * from tb_gps_data limit ?";
        String sql3 = "select * from tb_gps_data limit 10";
        ResultSet rst = null;
        Integer hascounts = 0;
        String sqlf = String.format(sql1, id);
        try {
            statement = conn.prepareStatement(sqlf + pcounts.toString());
            //statement.setString(1, "10");
            //statement.execute();
            rst = statement.executeQuery();
            loops++;
            System.out.println("search loop " + loops);
            while(rst.next()) {
                hascounts++;
                System.out.println("daddaaaaa" + rst.getObject(1).getClass().getName());
                id = rst.getLong(1);
                System.out.print(rst.getLong(1));
                //if(rst.getObject(1).toString().equals("232949626695716871") ) System.out.println ("find");
                System.out.print("\t"+rst.getString(2));
                System.out.print("\t"+rst.getString(3));
                System.out.print("\t"+rst.getString(4));
                System.out.println("\t"+ String.valueOf(rst.getDouble(5)));

                System.out.print("\t"+ rst.getInt(6));//latitude_flag
                System.out.print("\t"+ String.valueOf(rst.getDouble(7)));
                System.out.print("\t"+ rst.getInt(8));
                System.out.print("\t"+ String.valueOf(rst.getFloat(9)));
                System.out.println("\t"+ String.valueOf(rst.getFloat(10)));

                System.out.print("\t"+ String.valueOf(rst.getFloat(11))); //azimuth
                System.out.print("\t"+rst.getString(12));
                System.out.print("\t"+rst.getString(13));
                System.out.print("\t"+rst.getString(14));
                System.out.println("\t"+rst.getString(15));

                System.out.print("\t"+rst.getString(16));  //mcc
                System.out.print("\t"+rst.getString(17));
                System.out.print("\t"+ String.valueOf(rst.getFloat(18)));
                System.out.print("\t"+ String.valueOf(rst.getFloat(19)));
                System.out.println("\t"+rst.getString(20));

                System.out.print("\t"+rst.getTimestamp(21));
                System.out.println("\t"+rst.getTimestamp(22));


            }

            if(pcounts != hascounts){
                errstop = true;
                System.out.println("Warning: counts below limits " + String.valueOf(hascounts));
            }

            statement.close();
        }catch(SQLException e){

        }finally {

        }
    }
    void close() {
        try {
            statement.close();
            conn.close();

        }catch(SQLException e){

        }finally {

        }
    }

}
